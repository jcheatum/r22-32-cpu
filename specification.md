# R22-32 CPU Specification

## Overview

- RISC Load-store ISA
- Little-endian
- 32-bit data width
- 32-bit address width
- 15 general purpose registers
- Memory-mapped I/O


## Instruction set

Instruction                 Name                        Opcode      Type        Description
--------------------        -----                       -------     -----       ----------------------------
`nop`                       No-operation                `0x00`      DC\*        Do nothing
`mov   rD, rA`              Move                        `0x02`      R           `Reg[rD] = Reg[rA]`
`mvi   rD, immed`           Move immediate              `0x03`      I           `Reg[rD] = immed`
`halt`                      Halt execution              `0x01`      DC\*           
`push  rA`                  Stack Push                  `0x04`      R           `Stack[SP] = Reg[rA]; SP++`
`pop   rD`                  Stack Pop                   `0x05`      R           `SP--; Reg[rD] = Stack[SP]`
`lui   rD, immed`           Load upper immediate        `0x06`      I           `Reg[rD] = immed[15:0] << 16`
`ldb   rD, rA`              Load byte                   `0x08`      R           `Reg[rD] (lowest) = Mem[Reg[rA]]`
`ldh   rD, rA`              Load half-word              `0x0A`      R           `Reg[rD] (lower) = Mem[Reg[rA]] (2 bytes)`
`ldw   rD, rA`              Load word                   `0x0B`      R           `Reg[rD] = Mem[Reg[rA]] (4 bytes)`
`stb   rB, rA`              Store byte                  `0x0C`      R           `Mem[Reg[rA]] (byte) = Reg[rB] (lowest)`
`sth   rB, rA`              Store half-word             `0x0E`      R           `Mem[Reg[rA]] (2 bytes) = Reg[rB] (lower)`
`stw   rB, rA`              Store word                  `0x0F`      R           `Mem[Reg[rA]] (4 bytes) = Reg[rB]` 
`add   rD, rA, rB`          Add                         `0x18`      R           `Reg[rD] = Reg[rA] + Reg[rB]`
`sub   rD, rA, rB`          Subtract                    `0x1C`      R           `Reg[rD] = Reg[rA] - Reg[rB]`
`and   rD, rA, rB`          Bitwise AND                 `0x21`      R           `Reg[rD] = Reg[rA] & Reg[rB]`
`or    rD, rA, rB`          Bitwise OR                  `0x22`      R           `Reg[rD] = Reg[rA] | Reg[rB]`
`xor   rD, rA, rB`          Bitwise XOR                 `0x23`      R           `Reg[rD] = Reg[rA] ^ Reg[rB]`
`not   rD, rA`              Bitwise NOT                 `0x24`      R           `Reg[rD] = ~Reg[rA]`
`nand  rD, rA, rB`          Bitwise NAND                `0x25`      R           `Reg[rD] = ~(Reg[rA] & Reg[rB])`
`nor   rD, rA, rB`          Bitwise NOR                 `0x26`      R           `Reg[rD] = ~(Reg[rA] | Reg[rB])`
`xnor  rD, rA, rB`          Bitwise XNOR                `0x27`      R           `Reg[rD] = ~(Reg[rA] ^ Reg[rB])`
`addi  rD, rA, immed`       Add immediate               `0x19`      RI          `Reg[rD] = Reg[rA] + SignExtImmed`
`mul   rD, rA, rB`          Multiply                    `0x1A`      R           `Reg[rD] = Reg[rA] * Reg[rB]`
`muli  rD, rA, immed`       Multiply immediate          `0x1B`      RI          `Reg[rD] = Reg[rA] * SignExtImmed`
`div   rD, rA, rB`          Divide                      `0x1E`      R           `Reg[rD] = Reg[rA] / Reg[rB]`
`divi  rD, rA, immed`       Divide immediate            `0x1F`      RI          `Reg[rD] = Reg[rA] / SignExtImmed`
`sl    rD, rA, rB`          Shift left                  `0x28`      R           `Reg[rD] = Reg[rB] << Reg[rA]`
`sli   rD, rA, immed`       Shift left immediate        `0x2A`      RI          `Reg[rD] = Reg[rB] << ZeroExtImmed`
`srl   rD, rA, rB`          Shift right logical         `0x2C`      R           `Reg[rD] = Reg[rA] >> Reg[rB]`
`sra   rD, rA, rB`          Shift right arithmetic      `0x2D`      R           `Reg[rD] = Reg[rA] >>> Reg[rB]`
`srli  rD, rA, immed`       Shift right log. immed.     `0x2E`      RI          `Reg[rD] = Reg[rB] >> ZeroExtImmed`
`srai  rD, rA, immed`       Shift right arith. immed.   `0x2F`      RI          `Reg[rD] = Reg[rB] >>> ZeroExtImmed`
`jmp   label`               Jump                        `0x10`      J           `PC += JumpAddr`
`call  label`               Call function               `0x11`      J           `push ReturnAddr; PC += JumpAddr`
`jr    rA`                  Jump register               `0x12`      R           `PC = Reg[rA]`
`calr  rA`                  Call register               `0x13`      R           `push ReturnAddr; PC = Reg[rA]`
`jeq   rA, rB, label`       Jump if equal               `0x14`      R           `if(Reg[rA] == Reg[rB]) goto label`
`jne   rA, rB, label`       Jump if not equal           `0x15`      R           `if(Reg[rA] != Reg[rB]) goto label`
`jlt   rA, rB, label`       Jump if less than           `0x16`      R           `if(Reg[rA] < Reg[rB]) goto label`
`jge   rA, rB, label`       Jump if greater or equal    `0x17`      R           `if(Reg[rA] >= Reg[rB]) goto label`
`addf  rD, rA, rB`          Add float                   `0x30`      R           `Reg[rD] = (float) Reg[rA] + (float) Reg[rB]`
`mulf  rD, rA, rB`          Multiply float              `0x31`      R           `Reg[rD] = (float) Reg[rA] * (float) Reg[rB]`
`subf  rD, rA, rB`          Subtract float              `0x32`      R           `Reg[rD] = (float) Reg[rA] - (float) Reg[rB]`
`divf  rD, rA, rB`          Divide float                `0x33`      R           `Reg[rD] = (float) Reg[rA] / (float) Reg[rB]`
`upcf  rA`                  Upcast float                `0x34`      R           `LFR = (double) Reg[rA]`
`addlf`                     Add long float              `0x38`      DC\*        `LFR = LFA + LFB`
`mullf`                     Multiply long float         `0x39`      DC\*        `LFR = LFA * LFB`
`sublf`                     Subtract long float         `0x3A`      DC\*        `LFR = LFA - LFB`
`divlf`                     Divide long float           `0x3B`      DC\*        `LFR = LFA / LFB`

\*DC = "Don't Care".


## Instruction format

    R-format:  | Opcode (31:24) | rD (23:20) | rA (19:16) | rB (15:12) | Immed (11:0)   |
    RI-format: | Opcode (31:24) | rD (23:20) | rA (19:16) | Immed (15:0)                |
    I-Format:  | Opcode (31:24) | rD (23:20) | Immed (19:0)                             |  
    J-Format:  | Opcode (31:24) | JumpAddr (23:0)                                       |  


## Registers

- Register file
    - `$0`: Constant zero
    - `$1` - `$15`: General purpose registers
        - `$1`: Return value register `$v`
        - `$2` - `$5`: Argument registers `$a0` - `$a3`
        - `$6` - `$7`: Assembler temporary registers `$at0` - `$at1`
        - `$8` - `$15`: General use registers `$r0` - `$r7` 
- `SP`: Stack pointer. Written directly by `call` and `calr` instructions. Not directly writable by the programmer. Readable through memory mapping.
- `PC`: Program counter. Written and read directly by jump/call instructions and instruction fetching. Not directly accessible to the programmer.
- `MB`: Memory bank register. Memory-mapped register. Determines which memory bank is used when accessing general purpose banked memory.
- `BA`: Banks available register. Read-only memory-mapped register. Contains the number of available banks that are present. The value in `MB` should not exceed `BA`.
- `LFA`: Long float input A. 64-bit memory-mapped register. Holds input for the 64-bit floating point unit. Split into LFAL (lower) and LFAU (upper).
- `LFB`: Long float input B. 64-bit memory-mapped register. Holds input for the 64-bit floating point unit. Split into LFBL (lower) and LFBU (upper).
- `LFR`: Long float result. 64-bit read-only memory-mapped register. Holds output of the 64-bit floating point unit. Split into LFRL (lower) and LFRU (upper).


## Memory Layout

Address Range                   Usage
--------------------------      -----------------------------
`0x00000000 - 0x00007FFF`       General Purpose ROM
`0x00008000 - 0xBFFFFFFF`       General Purpose RAM (Banked)
`0xC0000000 - 0xFEFFFFFF`       Stack
`0xFF000000 - 0xFFFFFFDA`       General Purpose I/O 
`0xFFFFFFDB - 0xFFFFFFDF`       LFAL Register
`0xFFFFFFE0 - 0xFFFFFFE3`       LFAU Register
`0xFFFFFFE4 - 0xFFFFFFE7`       LFBL Register
`0xFFFFFFE8 - 0xFFFFFFEB`       LFBU Register
`0xFFFFFFEC - 0xFFFFFFEF`       LFRL Register (Read only)
`0xFFFFFFF0 - 0xFFFFFFF3`       LFRU Register (Read only)
`0xFFFFFFF4 - 0xFFFFFFF7`       SP Register (Read only)
`0xFFFFFFF8 - 0xFFFFFFFB`       MB Register
`0xFFFFFFFC - 0xFFFFFFFF`       BA Register (Read only)


## Memory Banking

The memory locations at addresses `0x00008000` through `0xBFFFFFFF` can be swapped for a different bank by changing the value in the `MB` register. This effectively means that for addresses in that range the true address of the data being accessed is its memory address concatenated to the value in `MB`, for a theoretical maximum of 13834917313498841088 bytes (~12 EiB) of general purpose ram. The `MB` register has no effect on the addressing of any memory outside of the banked address range. The `BA` (Banks Available) register can be read to determine the maximum value that should be used for `MB` in the current hardware configuration.


## Startup procedure

On startup (or reset), the program counter will be set to `0x00000000` and the processor will begin executing instructions from the beginning of ROM. Typically, the ROM would contain some sort of firmware that would load a program into RAM and jump to it, but the details of this are beyond the scope of this specification and the ROM may contain anything, so long as the beginning of ROM is a sequence of instructions.

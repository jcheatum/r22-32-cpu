fn main() {
    // run with `--serial 0xFF000000`
    let mut rom_instructions = Vec::<u32>::new();

    rom_instructions.push(0x03800041); // mvi $r0, 'A'
    rom_instructions.push(0x03A0001A); // mvi $r2, 26
    //rom_instructions.push(0x039000FF); // mvi $r1, 0xFF
    //rom_instructions.push(0x2A990018); // sli $r1, $r1, 24
    rom_instructions.push(0x0690FF00); // lui $r1, 0xFF00
    rom_instructions.push(0x0F098000); // L0: stw $r0, $r1
    rom_instructions.push(0x0F090000); //     stw $0, $r1
    rom_instructions.push(0x19880001); //     addi $r0, $r0, 1
    rom_instructions.push(0x19AAFFFF); //     addi $r2, $r2, -1
    rom_instructions.push(0x150A0FF0); //     jne $r2, $0, L0
    rom_instructions.push(0x01000000); // halt

    let mut rom = Vec::<u8>::new();
    for i in rom_instructions.iter() {
        let bytes = i.to_le_bytes();
        for &b in bytes.iter() {
            rom.push(b);
        }
    }

    while rom.len() < 0x8000 {
        rom.push(0);
    }

    std::fs::write("loops.bin", rom);
}
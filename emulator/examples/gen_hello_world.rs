fn main() {
    // run with `--serial 0xFF000000`
    let mut instructions = Vec::<u32>::new();
    instructions.push(0x03800048); // mvi $r0, 'H'
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x03800065); // mvi $r0, 'e'
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x0380006C); // mvi $r0, 'l'
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x00000000); // nop
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x0380006F); // mvi $r0, 'o'
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x0380002C); // mvi $r0, ','
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x03800020); // mvi $r0, ' '
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x03800057); // mvi $r0, 'W'
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x0380006F); // mvi $r0, 'o'
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x03800072); // mvi $r0, 'r'
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x0380006C); // mvi $r0, 'l'
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x03800064); // mvi $r0, 'd'
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x03800021); // mvi $r0, '!'
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x0380000A); // mvi $r0, '\n'
    instructions.push(0x039000FF); // mvi $r1, 0xFF
    instructions.push(0x2A990018); // sli $r1, $r1, 24
    instructions.push(0x0F098000); // stw $r0, $r1
    instructions.push(0x0F090000); // stw $0, $r1

    instructions.push(0x01000000); // halt

    let mut rom = Vec::<u8>::new();
    for i in instructions.iter() {
        let bytes = i.to_le_bytes();
        for &b in bytes.iter() {
            rom.push(b);
        }
    }

    while rom.len() < 0x8000 {
        rom.push(0);
    }

    std::fs::write("hello_world.bin", rom);
}
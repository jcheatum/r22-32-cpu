/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use std::{env, process, thread};

use getopts::Options;

mod emu;

fn main() {
    let argv: Vec<String> = env::args().collect();

    let mut opts = Options::new();
    opts.optflag(
        "h",
        "help",
        "Print this help message and exit."
    );
    opts.optopt(
        "r",
        "rom",
        "Specify 32 KiB (32768 bytes) binary file to use as ROM.",
        "ROMFILE"
    );
    opts.optopt(
        "s",
        "serial",
        "Specify an address between 0xFF000000 and 0xFFFFFFDA to be used as a serial connection to print ASCII characters to stdout. Note that this is not meant to emulate the full functionality of a real serial connection and is just for debugging purposes.",
        "ADDR"
    );
    opts.optopt(
        "b",
        "banks",
        "Specify the number of memory banks to use. The default is 2.",
        "N"
    );

    let matches = match opts.parse(&argv[1..]) {
        Ok(m)   =>  m,
        Err(e)  =>  {
            eprintln!("{}", e);
            process::exit(1);
        }
    };

    if matches.opt_present("h") {
        print!("{}", opts.usage("Usage: emulator [options]"));
        process::exit(0);
    }
    let rom_filename = matches.opt_str("r");
    let serial_addr = if let Some(optaddr) = matches.opt_str("s") {
        Some(u32::from_str_radix(optaddr.as_str(), 16).expect("Error: Invalid address"))
    } else {
        None
    };
    let num_banks = if let Ok(Some(n)) = matches.opt_get::<u32>("b") {
        n
    } else {
        2
    };

    let builder = thread::Builder::new()
        .name("run".into())
        .stack_size(2 * 1024 * 1024 * 1024); // 2 GB of stack space

    let handler = builder.spawn(move || {
        match emu::run(rom_filename, serial_addr, num_banks) {
            Ok(_)   =>  (),
            Err(e)  =>  {
                eprintln!("Error: {}", e);
                process::exit(1);
            }
        }
    }).unwrap();
    handler.join().unwrap();
}

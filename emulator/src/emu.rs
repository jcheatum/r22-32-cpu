/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use std::fs;

use anyhow::{anyhow, Result};
use byteorder::{ByteOrder, LittleEndian};
use r22_32::Instruction;

struct Memory {
    rom: Box<[u8; 0x8000]>,
    ram: Box<Vec<[u8; 0xBFFF7FFF]>>,
    stack: Box<[u8; 0x3EFFFFFF]>,
    gpio: Box<[u8; 0x00FFFFDA]>,
    lfal: u32,
    lfau: u32,
    lfbl: u32,
    lfbu: u32,
    lfrl: u32,
    lfru: u32,
    sp: u32,
    mb: u32,
    ba: u32
}

struct RegFile {
    registers: [u32; 15]
}

impl Memory {
    pub fn new(rom: Box<[u8; 0x8000]>, banks: u32) -> Self {
        Memory {
            rom,
            ram: Box::new(Vec::with_capacity(banks as usize)),
            stack: Box::new([0; 0x3EFFFFFF]),
            gpio: Box::new([0; 0x00FFFFDA]),
            lfal: 0,
            lfau: 0,
            lfbl: 0,
            lfbu: 0,
            lfrl: 0,
            lfru: 0,
            sp: 0xC0000000,
            mb: 0,
            ba: banks
        }
    }

    fn get_mmreg(addr: u32, base_addr: u32, reg_val: u32, num_bytes: usize) -> Result<Vec<u8>> {
        let mut buf = [0; 4];
        LittleEndian::write_u32(&mut buf, reg_val);
        
        Ok(if (addr - base_addr) as usize + num_bytes <= 4 {
            Vec::from(&buf[(addr - base_addr) as usize..((addr - base_addr) as usize + num_bytes)])
        } else {
            return Err(anyhow!("Error: Register read out of bounds."));
        })
    }

    fn set_mmreg(addr: u32, base_addr: u32, reg: &mut u32, bytes: Vec<u8>) -> Result<()> {
        if (addr - base_addr) as usize + bytes.len() <= 4 {
            let mut buf = [0; 4];
            for (i, &b) in bytes.iter().enumerate() {
                buf[(addr - base_addr) as usize + i] = b;
            }
            *reg = LittleEndian::read_u32(&buf);
            Ok(())
        } else {
            Err(anyhow!("Error: Register read out of bounds."))
        }
    }

    fn get(&self, addr: u32, num_bytes: usize) -> Result<Vec<u8>> {
        Ok(match addr {
            0x00000000..=0x00007FFF     =>  Vec::from(&self.rom[(addr as usize)..(addr as usize + num_bytes)]),
            0x00008000..=0xBFFFFFFF     =>  Vec::from(&self.ram.get(self.mb as usize).unwrap()[(addr as usize - 0x8000)..(addr as usize - 0x8000 + num_bytes)]),
            0xC0000000..=0xFEFFFFFF     =>  Vec::from(&self.stack[(addr as usize - 0xC0000000)..(addr as usize - 0xC0000000 + num_bytes)]),
            0xFF000000..=0xFFFFFFDA     =>  Vec::from(&self.gpio[(addr as usize - 0xFF000000)..(addr as usize - 0xFF000000 + num_bytes)]),
            0xFFFFFFDB..=0xFFFFFFDF     =>  Self::get_mmreg(addr, 0xFFFFFFDB, self.lfal, num_bytes)?,
            0xFFFFFFE0..=0xFFFFFFE3     =>  Self::get_mmreg(addr, 0xFFFFFFE0, self.lfau, num_bytes)?,
            0xFFFFFFE4..=0xFFFFFFE7     =>  Self::get_mmreg(addr, 0xFFFFFFE4, self.lfbl, num_bytes)?,
            0xFFFFFFE8..=0xFFFFFFEB     =>  Self::get_mmreg(addr, 0xFFFFFFE8, self.lfbu, num_bytes)?,
            0xFFFFFFEC..=0xFFFFFFEF     =>  Self::get_mmreg(addr, 0xFFFFFFEC, self.lfrl, num_bytes)?,
            0xFFFFFFF0..=0xFFFFFFF3     =>  Self::get_mmreg(addr, 0xFFFFFFF0, self.lfru, num_bytes)?,
            0xFFFFFFF4..=0xFFFFFFF7     =>  Self::get_mmreg(addr, 0xFFFFFFF4, self.sp, num_bytes)?,
            0xFFFFFFF8..=0xFFFFFFFB     =>  Self::get_mmreg(addr, 0xFFFFFFF8, self.mb, num_bytes)?,
            0xFFFFFFFC..=0xFFFFFFFF     =>  Self::get_mmreg(addr, 0xFFFFFFFC, self.ba, num_bytes)?
        })
    }

    pub fn get_byte(&self, addr: u32) -> Result<u8> {
        Ok(*self.get(addr, 1)?.get(0).unwrap())
    }

    pub fn get_half(&self, addr: u32) -> Result<u16> {
        Ok(LittleEndian::read_u16(&self.get(addr, 2)?))
    }

    pub fn get_word(&self, addr: u32) -> Result<u32> {
        Ok(LittleEndian::read_u32(&self.get(addr, 4)?))
    }

    fn set(&mut self, addr: u32, bytes: Vec<u8>) -> Result<()> {
        match addr {
            // Do nothing when attempting to write to read only areas.
            0x00000000..=0x00007FFF | 0xFFFFFFEC..=0xFFFFFFF7 | 0xFFFFFFFC..=0xFFFFFFFF =>  Ok(()),

            0x00008000..=0xBFFFFFFF =>  {
                for i in 0..bytes.len() {
                    self.ram[self.mb as usize][addr as usize - 0x8000 + i] = bytes[i];
                }
                Ok(())
            },

            0xC0000000..=0xFEFFFFFF =>  {
                for i in 0..bytes.len() {
                    self.stack[addr as usize - 0xC0000000 + i] = bytes[i];
                }
                Ok(())
            },

            0xFF000000..=0xFFFFFFDA =>  {
                for i in 0..bytes.len() {
                    self.gpio[addr as usize - 0xFF000000 + i] = bytes[i];
                }
                Ok(())
            },

            0xFFFFFFDB..=0xFFFFFFDF =>  { Self::set_mmreg(addr, 0xFFFFFFDB, &mut self.lfal, bytes)?; Ok(()) },
            0xFFFFFFE0..=0xFFFFFFE3 =>  { Self::set_mmreg(addr, 0xFFFFFFE0, &mut self.lfau, bytes)?; Ok(()) },
            0xFFFFFFE4..=0xFFFFFFE7 =>  { Self::set_mmreg(addr, 0xFFFFFFE4, &mut self.lfbl, bytes)?; Ok(()) },
            0xFFFFFFE8..=0xFFFFFFEB =>  { Self::set_mmreg(addr, 0xFFFFFFE8, &mut self.lfbu, bytes)?; Ok(()) },
            0xFFFFFFF8..=0xFFFFFFFB =>  { Self::set_mmreg(addr, 0xFFFFFFF8, &mut self.mb, bytes)?; Ok(()) }
        }
    }

    pub fn set_byte(&mut self, addr: u32, data: u8) -> Result<()> {
        self.set(addr, vec![data])
    }

    pub fn set_half(&mut self, addr: u32, data: u16) -> Result<()> {
        let mut buf = [0; 2];
        LittleEndian::write_u16(&mut buf, data);
        self.set(addr, Vec::from(buf))
    }

    pub fn set_word(&mut self, addr: u32, data: u32) -> Result<()> {
        let mut buf = [0; 4];
        LittleEndian::write_u32(&mut buf, data);
        self.set(addr, Vec::from(buf))
    }
}

impl RegFile {
    pub fn new() -> Self {
        RegFile {
            registers: [0; 15]
        }
    }

    pub fn get(&self, ra: usize, rb: usize) -> (u32, u32) {
        (
            if ra == 0 { 0 } else { self.registers[ra - 1] },
            if rb == 0 { 0 } else { self.registers[rb - 1] }
        )
    }

    pub fn set(&mut self, rd: usize, value: u32) {
        if rd != 0 {
            self.registers[rd - 1] = value;
        }
    }
}

pub fn run(rom_filename: Option<String>, serial_addr: Option<u32>, banks: u32) -> Result<()> {
    eprintln!("Running program");
    let rom: Box<[u8; 0x8000]> = Box::new(if let Some(filename) = rom_filename {
        let mut file = fs::read(filename)?;
        if file.len() > 0x8000 {
            eprintln!("Warning: Romfile is larger than ROM. Using first 32768 bytes.");
            file[..0x8000].try_into()?
        } else if file.len() == 0x8000 {
            file.as_slice().try_into()?
        } else { // if file.len() < 0x8000
            while file.len() < 0x8000 {
                file.push(0);
            }
            file.as_slice().try_into()?
        }
    } else {
        eprintln!("Warning: No romfile provided. Using empty ROM.");
        [0; 0x8000]
    });
    eprintln!("Loaded ROM");
    let mut mem = Memory::new(rom, banks);
    eprintln!("Created Memory");
    let mut regfile = RegFile::new();
    let mut program_counter: u32 = 0x00000000;

    loop {
        let mb_save = mem.mb;
        mem.mb = 0;
        let instr = Instruction::from_machine_code(mem.get_word(program_counter)?)?;
        mem.mb = mb_save;
        let mut jmp = false;
        
        match instr {
            Instruction::NOP    =>  (),
            Instruction::MOV {rd, ra}   =>  regfile.set(rd as usize, regfile.get(ra as usize, 0).0),
            Instruction::MVI {rd, immed}   =>  regfile.set(rd as usize, immed),
            Instruction::HALT   =>  {
                println!("Program execution halted");
                print_reg_contents(&regfile, &mem);
                println!("Exiting");
                return Ok(())
            },
            Instruction::PUSH {ra}  =>  {
                mem.set_word(mem.sp, regfile.get(ra as usize, 0).0)?;
                mem.sp += 4;
            },
            Instruction::POP {rd}   =>  {
                mem.sp -= 4;
                regfile.set(rd as usize, mem.get_word(mem.sp)?);
            },
            Instruction::LUI {rd, immed}    =>  {
                regfile.set(rd as usize, immed << 16);
            },
            Instruction::LDB {rd, ra}   =>  {
                let addr = regfile.get(ra as usize, 0).0;
                regfile.set(rd as usize, mem.get_byte(addr)? as u32);
            },
            Instruction::LDH {rd, ra}   =>  {
                let (addr, _) = regfile.get(ra as usize, 0);
                regfile.set(rd as usize, mem.get_half(addr)? as u32);
            },
            Instruction::LDW {rd, ra}   =>  {
                let addr = regfile.get(ra as usize, 0).0;
                regfile.set(rd as usize, mem.get_word(addr)?);
            },
            Instruction::STB {rb, ra}   =>  {
                let (addr, value) = regfile.get(ra as usize, rb as usize);
                let mut buf = [0; 4];
                LittleEndian::write_u32(&mut buf, value);
                mem.set_byte(addr, buf[0])?;
            },
            Instruction::STH {rb, ra}   =>   {
                let (addr, value) = regfile.get(ra as usize, rb as usize);
                let mut buf = [0; 4];
                LittleEndian::write_u32(&mut buf, value);
                mem.set_half(addr, LittleEndian::read_u16(&mut buf[..2]))?;
            },
            Instruction::STW {rb, ra}   =>  {
                let (addr, value) = regfile.get(ra as usize, rb as usize);
                mem.set_word(addr, value)?;
            },
            Instruction::ADD {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                regfile.set(rd as usize, a.wrapping_add(b));
            },
            Instruction::SUB {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                regfile.set(rd as usize, a.wrapping_sub(b));
            },
            Instruction::AND {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                regfile.set(rd as usize, a & b);
            },
            Instruction::OR {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                regfile.set(rd as usize, a | b);
            },
            Instruction::XOR {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                regfile.set(rd as usize, a ^ b);
            },
            Instruction::NOT {rd, ra}   =>  {
                let (a, _) = regfile.get(ra as usize, 0);
                regfile.set(rd as usize, !a);
            },
            Instruction::NAND {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                regfile.set(rd as usize, !(a & b));
            },
            Instruction::NOR {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                regfile.set(rd as usize, !(a | b));
            },
            Instruction::XNOR {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                regfile.set(rd as usize, !(a ^ b));
            },
            Instruction::ADDI {rd, ra, immed}   =>  {
                let (a, _) = regfile.get(ra as usize, 0);
                regfile.set(rd as usize, a.wrapping_add(sign_extend_16(immed)))
            },
            Instruction::MUL {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                let s_a = i32::from_le_bytes(a.to_le_bytes());
                let s_b = i32::from_le_bytes(b.to_le_bytes());
                let prod = u32::from_le_bytes((s_a * s_b).to_le_bytes());
                regfile.set(rd as usize, prod);
            },
            Instruction::MULI {rd, ra, immed}   =>  {
                let (a, _) = regfile.get(ra as usize, 0);
                let s_a = i32::from_le_bytes(a.to_le_bytes());
                let b = sign_extend_16(immed);
                let s_b = i32::from_le_bytes(b.to_le_bytes());
                let prod = u32::from_le_bytes((s_a * s_b).to_le_bytes());
                regfile.set(rd as usize, prod);
            },
            Instruction::DIV {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                let s_a = i32::from_le_bytes(a.to_le_bytes());
                let s_b = i32::from_le_bytes(b.to_le_bytes());
                let prod = u32::from_le_bytes((s_a / s_b).to_le_bytes());
                regfile.set(rd as usize, prod);
            },
            Instruction::DIVI {rd, ra, immed}   =>  {
                let (a, _) = regfile.get(ra as usize, 0);
                let s_a = i32::from_le_bytes(a.to_le_bytes());
                let b = sign_extend_16(immed);
                let s_b = i32::from_le_bytes(b.to_le_bytes());
                let prod = u32::from_le_bytes((s_a / s_b).to_le_bytes());
                regfile.set(rd as usize, prod);
            },
            Instruction::SL {rd, ra, rb}    =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                regfile.set(rd as usize, a << b);
            },
            Instruction::SLI {rd, ra, immed}    =>  {
                let (a, _) = regfile.get(ra as usize, 0);
                regfile.set(rd as usize, a << immed);
            },
            Instruction::SRL {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                regfile.set(rd as usize, a >> b);
            },
            Instruction::SRA {rd, ra, rb}   =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                let s_a = i32::from_le_bytes(a.to_le_bytes()); // rust uses arithmetic shift right on signed ints
                regfile.set(rd as usize, u32::from_le_bytes((s_a >> b).to_le_bytes()));
            },
            Instruction::SRLI {rd, ra, immed}   =>  {
                let (a, _) = regfile.get(ra as usize, 0);
                regfile.set(rd as usize, a >> immed);
            },
            Instruction::SRAI {rd, ra, immed}   =>  {
                let (a, _) = regfile.get(ra as usize, 0);
                let s_a = i32::from_le_bytes(a.to_le_bytes());
                regfile.set(rd as usize, u32::from_le_bytes((s_a >> immed).to_le_bytes()));
            },
            Instruction::JMP {offset}   =>  {
                program_counter = program_counter.wrapping_add(sign_extend_24(offset));
                jmp = true;
            },
            Instruction::CALL {offset}  =>  {
                mem.set_word(mem.sp, program_counter)?;
                mem.sp += 4;
                program_counter = program_counter.wrapping_add(sign_extend_24(offset));
                jmp = true;
            },
            Instruction::JR {ra}    =>  {
                let (a, _) = regfile.get(ra as usize, 0);
                program_counter = a;
            },
            Instruction::CALR {ra}  =>  {
                mem.set_word(mem.sp, program_counter)?;
                mem.sp += 4;
                let (a, _) = regfile.get(ra as usize, 0);
                program_counter = a;
            },
            Instruction::JEQ {ra, rb, immed}    =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                if a == b {
                    program_counter = program_counter.wrapping_add(sign_extend_12(immed));
                    jmp = true;
                }
            },
            Instruction::JNE {ra, rb, immed}    =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                if a != b {
                    program_counter = program_counter.wrapping_add(sign_extend_12(immed));
                    jmp = true;
                }
            },
            Instruction::JLT {ra, rb, immed}    =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                if a < b {
                    program_counter = program_counter.wrapping_add(sign_extend_12(immed));
                    jmp = true;
                }
            },
            Instruction::JGE {ra, rb, immed}    =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                if a >= b {
                    program_counter = program_counter.wrapping_add(sign_extend_12(immed));
                    jmp = true;
                }
            },
            Instruction::ADDF {rd, ra, rb}  =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                let fa = f32::from_le_bytes(a.to_le_bytes());
                let fb = f32::from_le_bytes(b.to_le_bytes());
                regfile.set(rd as usize, u32::from_le_bytes((fa + fb).to_le_bytes()));
            },
            Instruction::MULF {rd, ra, rb}  =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                let fa = f32::from_le_bytes(a.to_le_bytes());
                let fb = f32::from_le_bytes(b.to_le_bytes());
                regfile.set(rd as usize, u32::from_le_bytes((fa * fb).to_le_bytes()));
            },
            Instruction::SUBF {rd, ra, rb}  =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                let fa = f32::from_le_bytes(a.to_le_bytes());
                let fb = f32::from_le_bytes(b.to_le_bytes());
                regfile.set(rd as usize, u32::from_le_bytes((fa - fb).to_le_bytes()));
            },
            Instruction::DIVF {rd, ra, rb}  =>  {
                let (a, b) = regfile.get(ra as usize, rb as usize);
                let fa = f32::from_le_bytes(a.to_le_bytes());
                let fb = f32::from_le_bytes(b.to_le_bytes());
                regfile.set(rd as usize, u32::from_le_bytes((fa / fb).to_le_bytes()));
            },
            Instruction::UPCF {ra}  =>  {
                let (a, _) = regfile.get(ra as usize, 0);
                let fa = f32::from_le_bytes(a.to_le_bytes());
                let lfr = f64::from(fa);
                let lfr_bytes = lfr.to_le_bytes();
                mem.lfrl = u32::from_le_bytes(lfr_bytes[..4].try_into()?);
                mem.lfru = u32::from_le_bytes(lfr_bytes[4..].try_into()?);

            },
            Instruction::ADDLF  =>  {
                let lfa = f64::from_le_bytes([mem.lfal.to_le_bytes().as_slice(), mem.lfau.to_le_bytes().as_slice()].concat().as_slice().try_into()?);
                let lfb = f64::from_le_bytes([mem.lfbl.to_le_bytes().as_slice(), mem.lfbu.to_le_bytes().as_slice()].concat().as_slice().try_into()?);
                let lfr = lfa + lfb;
                let lfr_bytes = lfr.to_le_bytes();
                mem.lfrl = u32::from_le_bytes(lfr_bytes[..4].try_into()?);
                mem.lfru = u32::from_le_bytes(lfr_bytes[4..].try_into()?);
            },
            Instruction::MULLF  =>  {
                let lfa = f64::from_le_bytes([mem.lfal.to_le_bytes().as_slice(), mem.lfau.to_le_bytes().as_slice()].concat().as_slice().try_into()?);
                let lfb = f64::from_le_bytes([mem.lfbl.to_le_bytes().as_slice(), mem.lfbu.to_le_bytes().as_slice()].concat().as_slice().try_into()?);
                let lfr = lfa * lfb;
                let lfr_bytes = lfr.to_le_bytes();
                mem.lfrl = u32::from_le_bytes(lfr_bytes[..4].try_into()?);
                mem.lfru = u32::from_le_bytes(lfr_bytes[4..].try_into()?);
            },
            Instruction::SUBLF  =>  {
                let lfa = f64::from_le_bytes([mem.lfal.to_le_bytes().as_slice(), mem.lfau.to_le_bytes().as_slice()].concat().as_slice().try_into()?);
                let lfb = f64::from_le_bytes([mem.lfbl.to_le_bytes().as_slice(), mem.lfbu.to_le_bytes().as_slice()].concat().as_slice().try_into()?);
                let lfr = lfa - lfb;
                let lfr_bytes = lfr.to_le_bytes();
                mem.lfrl = u32::from_le_bytes(lfr_bytes[..4].try_into()?);
                mem.lfru = u32::from_le_bytes(lfr_bytes[4..].try_into()?);
            },
            Instruction::DIVLF  =>  {
                let lfa = f64::from_le_bytes([mem.lfal.to_le_bytes().as_slice(), mem.lfau.to_le_bytes().as_slice()].concat().as_slice().try_into()?);
                let lfb = f64::from_le_bytes([mem.lfbl.to_le_bytes().as_slice(), mem.lfbu.to_le_bytes().as_slice()].concat().as_slice().try_into()?);
                let lfr = lfa / lfb;
                let lfr_bytes = lfr.to_le_bytes();
                mem.lfrl = u32::from_le_bytes(lfr_bytes[..4].try_into()?);
                mem.lfru = u32::from_le_bytes(lfr_bytes[4..].try_into()?);
            },
        }

        if let Some(addr) = serial_addr {
            use std::io::Write;
            let n = mem.get_word(addr)?;
            if n != 0 {
                print!("{}", char::from_u32(n).unwrap_or('?'));
                std::io::stdout().flush()?;
            }
        }

        if !jmp { program_counter += 4; }
    }
}

fn print_reg_contents(regfile: &RegFile, mem: &Memory) {
    println!("Register contents:");
    for i in 1..16 {
        println!("\t{}:\t0x{:08x}", i, regfile.registers[i - 1]);
    }

    let lfa = ((mem.lfau as u64) << 32) | mem.lfal as u64;
    let lfb = ((mem.lfbu as u64) << 32) | mem.lfbl as u64;
    let lfr = ((mem.lfru as u64) << 32) | mem.lfrl as u64;
    println!("\tLFA:\t0x{:016x} ({})", lfa, f64::from_bits(lfa));
    println!("\tLFB:\t0x{:016x} ({})", lfb, f64::from_bits(lfb));
    println!("\tLFR:\t0x{:016x} ({})", lfr, f64::from_bits(lfr));
    println!("\tSP:\t0x{:08x}", mem.sp);
    println!("\tMB:\t0x{:08x}", mem.mb);
    println!("\tBA:\t0x{:08x}", mem.ba);
}

fn sign_extend_12(immed: u32) -> u32 {
    if immed & 0x00000800 != 0 {
        immed | 0xFFFFF000
    } else {
        immed
    }
}

fn sign_extend_16(immed: u32) -> u32 {
    if immed & 0x00008000 != 0 {
        immed | 0xFFFF0000
    } else {
        immed
    }
}

fn sign_extend_24(immed: u32) -> u32 {
    if immed & 0x00800000 != 0 {
        immed | 0xFF000000
    } else {
        immed
    }
}

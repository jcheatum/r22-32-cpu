/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use anyhow::{anyhow, Result};

pub enum Instruction {
    NOP,
    MOV {rd: u32, ra: u32},
    MVI {rd: u32, immed: u32},
    HALT,
    PUSH {ra: u32},
    POP {rd: u32},
    LUI {rd: u32, immed: u32},
    LDB {rd: u32, ra: u32},
    LDH {rd: u32, ra: u32},
    LDW {rd: u32, ra: u32},
    STB {rb: u32, ra: u32},
    STH {rb: u32, ra: u32},
    STW {rb: u32, ra: u32},
    ADD {rd: u32, ra: u32, rb: u32},
    SUB {rd: u32, ra: u32, rb: u32},
    AND {rd: u32, ra: u32, rb: u32},
    OR {rd: u32, ra: u32, rb: u32},
    XOR {rd: u32, ra: u32, rb: u32},
    NOT {rd: u32, ra: u32},
    NAND {rd: u32, ra: u32, rb: u32},
    NOR {rd: u32, ra: u32, rb: u32},
    XNOR {rd: u32, ra: u32, rb: u32},
    ADDI {rd: u32, ra: u32, immed: u32},
    MUL {rd: u32, ra: u32, rb: u32},
    MULI {rd: u32, ra: u32, immed: u32},
    DIV {rd: u32, ra: u32, rb: u32},
    DIVI {rd: u32, ra: u32, immed: u32},
    SL {rd: u32, ra: u32, rb: u32},
    SLI {rd: u32, ra: u32, immed: u32},
    SRL {rd: u32, ra: u32, rb: u32},
    SRA {rd: u32, ra: u32, rb: u32},
    SRLI {rd: u32, ra: u32, immed: u32},
    SRAI {rd: u32, ra: u32, immed: u32},
    JMP {offset: u32},
    CALL {offset: u32},
    JR {ra: u32},
    CALR {ra: u32},
    JEQ {ra: u32, rb: u32, immed: u32},
    JNE {ra: u32, rb: u32, immed: u32},
    JLT {ra: u32, rb: u32, immed: u32},
    JGE {ra: u32, rb: u32, immed: u32},
    ADDF {rd: u32, ra: u32, rb: u32},
    MULF {rd: u32, ra: u32, rb: u32},
    SUBF {rd: u32, ra: u32, rb: u32},
    DIVF {rd: u32, ra: u32, rb: u32},
    UPCF {ra: u32},
    ADDLF,
    MULLF,
    SUBLF,
    DIVLF,
}

impl Instruction {
    fn assemble_rtype(opcode: u32, rd: u32, ra: u32, rb: u32, immed: u32) -> Result<u32> {
        if opcode < 256 && rd < 16 && ra < 16 && rb < 16 && immed <= 0xFFF {
            Ok(opcode << 24 | rd << 20 | ra << 16 | rb << 12 | immed)
        } else {
            Err(anyhow!("One or more R-format parameters is out of bounds."))
        }
    }

    fn assemble_ritype(opcode: u32, rd: u32, ra: u32, immed: u32) -> Result<u32> {
        if opcode < 256 && rd < 16 && ra < 16 && immed <= 0xFFFF {
            Ok(opcode << 24 | rd << 20 | ra << 16 | immed)
        } else {
            Err(anyhow!("One or more RI-format parameters is out of bounds."))
        }
    }

    fn assemble_itype(opcode: u32, rd: u32, immed: u32) -> Result<u32> {
        if opcode < 256 && rd < 16 && immed <= 0xFFFFF {
            Ok(opcode << 24 | rd << 20 | immed)
        } else {
            Err(anyhow!("One or more I-format parameters is out of bounds."))
        }
    }

    fn assemble_jtype(opcode: u32, jumpaddr: u32) -> Result<u32> {
        if opcode < 256 && jumpaddr <= 0xFFFFFF {
            Ok(opcode << 24 | jumpaddr)
        } else {
            Err(anyhow!("One or more J-format parameters is out of bounds."))
        }
    }

    pub fn to_machine_code(&self) -> Result<u32> {
        match self {
            Self::NOP   =>  Ok(0x00000000),
            Self::MOV {rd, ra}  =>  Self::assemble_rtype(0x02, *rd, *ra, 0, 0),
            Self::MVI {rd, immed} =>  Self::assemble_itype(0x03, *rd, *immed),
            Self::HALT  =>  Ok(0x01000000),
            Self::PUSH {ra} =>  Self::assemble_rtype(0x04, 0, *ra, 0, 0),
            Self::POP {rd}  =>  Self::assemble_rtype(0x05, *rd, 0, 0, 0),
            Self::LUI {rd, immed}   =>  Self::assemble_itype(0x06, *rd, *immed),
            Self::LDB {rd, ra}  =>  Self::assemble_rtype(0x08, *rd, *ra, 0, 0),
            Self::LDH {rd, ra}  =>  Self::assemble_rtype(0x0A, *rd, *ra, 0, 0),
            Self::LDW {rd, ra}  =>  Self::assemble_rtype(0x0B, *rd, *ra, 0, 0),
            Self::STB {rb, ra}  =>  Self::assemble_rtype(0x0C, 0, *ra, *rb, 0),
            Self::STH {rb, ra}  =>  Self::assemble_rtype(0x0E, 0, *ra, *rb, 0),
            Self::STW {rb, ra}  =>  Self::assemble_rtype(0x0F, 0, *ra, *rb, 0),
            Self::ADD {rd, ra, rb}  =>  Self::assemble_rtype(0x18, *rd, *ra, *rb, 0),
            Self::SUB {rd, ra, rb}  =>  Self::assemble_rtype(0x1C, *rd, *ra, *rb, 0),
            Self::AND {rd, ra, rb}  =>  Self::assemble_rtype(0x21, *rd, *ra, *rb, 0),
            Self::OR {rd, ra, rb}   =>  Self::assemble_rtype(0x22, *rd, *ra, *rb, 0),
            Self::XOR {rd, ra, rb}  =>  Self::assemble_rtype(0x23, *rd, *ra, *rb, 0),
            Self::NOT {rd, ra}  =>  Self::assemble_rtype(0x24, *rd, *ra, 0, 0),
            Self::NAND {rd, ra, rb} =>  Self::assemble_rtype(0x25, *rd, *ra, *rb, 0),
            Self::NOR {rd, ra, rb}  =>  Self::assemble_rtype(0x26, *rd, *ra, *rb, 0),
            Self::XNOR {rd, ra, rb} =>  Self::assemble_rtype(0x27, *rd, *ra, *rb, 0),
            Self::ADDI {rd, ra, immed}  =>  Self::assemble_ritype(0x19, *rd, *ra, *immed),
            Self::MUL {rd, ra, rb}  =>  Self::assemble_rtype(0x1A, *rd, *ra, *rb, 0),
            Self::MULI {rd, ra, immed}  =>  Self::assemble_ritype(0x1B, *rd, *ra, *immed),
            Self::DIV {rd, ra, rb}  =>  Self::assemble_rtype(0x1E, *rd, *ra, *rb, 0),
            Self::DIVI {rd, ra, immed}  =>  Self::assemble_ritype(0x1F, *rd, *ra, *immed),
            Self::SL {rd, ra, rb}   =>  Self::assemble_rtype(0x28, *rd, *ra, *rb, 0),
            Self::SLI {rd, ra, immed}   =>  Self::assemble_ritype(0x2A, *rd, *ra, *immed),
            Self::SRL {rd, ra, rb}  =>  Self::assemble_rtype(0x2C, *rd, *ra, *rb, 0),
            Self::SRA {rd, ra, rb}  =>  Self::assemble_rtype(0x2D, *rd, *ra, *rb, 0),
            Self::SRLI {rd, ra, immed}  =>  Self::assemble_ritype(0x2E, *rd, *ra, *immed),
            Self::SRAI {rd, ra, immed}  =>  Self::assemble_ritype(0x2F, *rd, *ra, *immed),
            Self::JMP {offset}  =>  Self::assemble_jtype(0x10, *offset),
            Self::CALL {offset} =>  Self::assemble_jtype(0x11, *offset),
            Self::JR {ra}   =>  Self::assemble_rtype(0x12, 0, *ra, 0, 0),
            Self::CALR {ra} =>  Self::assemble_rtype(0x13, 0, *ra, 0, 0),
            Self::JEQ {ra, rb, immed}   =>  Self::assemble_rtype(0x14, 0, *ra, *rb, *immed),
            Self::JNE {ra, rb, immed}   =>  Self::assemble_rtype(0x15, 0, *ra, *rb, *immed),
            Self::JLT {ra, rb, immed}   =>  Self::assemble_rtype(0x16, 0, *ra, *rb, *immed),
            Self::JGE {ra, rb, immed}   =>  Self::assemble_rtype(0x17, 0, *ra, *rb, *immed),
            Self::ADDF {rd, ra, rb} =>  Self::assemble_rtype(0x30, *rd, *ra, *rb, 0),
            Self::MULF {rd, ra, rb} =>  Self::assemble_rtype(0x31, *rd, *ra, *rb, 0),
            Self::SUBF {rd, ra, rb} =>  Self::assemble_rtype(0x32, *rd, *ra, *rb, 0),
            Self::DIVF {rd, ra, rb} =>  Self::assemble_rtype(0x33, *rd, *ra, *rb, 0),
            Self::UPCF {ra} =>  Self::assemble_rtype(0x34, 0, *ra, 0, 0),
            Self::ADDLF =>  Ok(0x38000000),
            Self::MULLF =>  Ok(0x39000000),
            Self::SUBLF =>  Ok(0x3A000000),
            Self::DIVLF =>  Ok(0x3B000000),
        }
    }

    pub fn from_machine_code(instr: u32) -> Result<Self> {
        let opcode = (instr & 0xFF000000) >> 24;
        let rd = (instr & 0x00F00000) >> 20;
        let ra = (instr & 0x000F0000) >> 16;
        let rb = (instr & 0x0000F000) >> 12;
        let r_immed = instr & 0x00000FFF;
        let ri_immed = instr & 0x0000FFFF;
        let i_immed = instr & 0x000FFFFF;
        let j_immed = instr & 0x00FFFFFF;

        Ok(match opcode {
            0x00    =>  Self::NOP,
            0x02    =>  Self::MOV {rd, ra},
            0x03    =>  Self::MVI {rd, immed: i_immed},
            0x01    =>  Self::HALT,
            0x04    =>  Self::PUSH {ra},
            0x05    =>  Self::POP {rd},
            0x06    =>  Self::LUI {rd, immed: i_immed},
            0x08    =>  Self::LDB {rd, ra},
            0x0A    =>  Self::LDH {rd, ra},
            0x0B    =>  Self::LDW {rd, ra},
            0x0C    =>  Self::STB {rb, ra},
            0x0E    =>  Self::STH {rb, ra},
            0x0F    =>  Self::STW {rb, ra},
            0x18    =>  Self::ADD {rd, ra, rb},
            0x1C    =>  Self::SUB {rd, ra, rb},
            0x21    =>  Self::AND {rd, ra, rb},
            0x22    =>  Self::OR {rd, ra, rb},
            0x23    =>  Self::XOR {rd, ra, rb},
            0x24    =>  Self::NOT {rd, ra},
            0x25    =>  Self::NAND {rd, ra, rb},
            0x26    =>  Self::NOR {rd, ra, rb},
            0x27    =>  Self::XNOR {rd, ra, rb},
            0x19    =>  Self::ADDI {rd, ra, immed: ri_immed},
            0x1A    =>  Self::MUL {rd, ra, rb},
            0x1B    =>  Self::MULI {rd, ra, immed: ri_immed},
            0x1E    =>  Self::DIV {rd, ra, rb},
            0x1F    =>  Self::DIVI {rd, ra, immed: ri_immed},
            0x28    =>  Self::SL {rd, ra, rb},
            0x2A    =>  Self::SLI {rd, ra, immed: ri_immed},
            0x2C    =>  Self::SRL {rd, ra, rb},
            0x2D    =>  Self::SRA {rd, ra, rb},
            0x2E    =>  Self::SRLI {rd, ra, immed: ri_immed},
            0x2F    =>  Self::SRAI {rd, ra, immed: ri_immed},
            0x10    =>  Self::JMP {offset: j_immed},
            0x11    =>  Self::CALL {offset :j_immed},
            0x12    =>  Self::JR {ra},
            0x13    =>  Self::CALR {ra},
            0x14    =>  Self::JEQ {ra, rb, immed: r_immed},
            0x15    =>  Self::JNE {ra, rb, immed: r_immed},
            0x16    =>  Self::JLT {ra, rb, immed: r_immed},
            0x17    =>  Self::JGE {ra, rb, immed: r_immed},
            0x30    =>  Self::ADDF {rd, ra, rb},
            0x31    =>  Self::MULF {rd, ra, rb},
            0x32    =>  Self::SUBF {rd, ra, rb},
            0x33    =>  Self::DIVF {rd, ra, rb},
            0x34    =>  Self::UPCF {ra},
            0x38    =>  Self::ADDLF,
            0x39    =>  Self::MULLF,
            0x3A    =>  Self::SUBLF,
            0x3B    =>  Self::DIVLF,
            _       =>  { return Err(anyhow!("Invalid opcode: 0x{:x}", opcode)) }
        })
    }
}
